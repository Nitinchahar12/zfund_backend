from django.db import models
from zfund_backend.users.models import User

class Category(models.Model):
    name = models.CharField(max_length=255, unique=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=255)
    description = models.TextField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name

class Purchase(models.Model):
    advisor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='purchases')
    client = models.ForeignKey(User, on_delete=models.CASCADE, related_name='purchased_for')
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    unique_link = models.URLField()

    def __str__(self):
        return f"{self.advisor.name} bought {self.product.name} for {self.client.name}"
