from django.urls import path
from . import views

urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('add-client/', views.add_client, name='add_client'),
    path('view-clients/<int:advisor_id>/', views.view_clients, name='view_clients'),
    path('add-product/', views.add_product, name='add_product'),
    path('purchase-product/', views.purchase_product, name='purchase_product'),
]
