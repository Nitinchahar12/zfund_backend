# Generated by Django 4.2.4 on 2023-08-22 15:55

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        ("users", "0001_initial"),
    ]

    operations = [
        migrations.AddField(
            model_name="user",
            name="advisor",
            field=models.ForeignKey(
                null=True,
                on_delete=django.db.models.deletion.SET_NULL,
                related_name="clients",
                to=settings.AUTH_USER_MODEL,
            ),
        ),
        migrations.AddField(
            model_name="user",
            name="mobile",
            field=models.CharField(default=91, max_length=15, unique=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="user",
            name="otp",
            field=models.CharField(default="******", max_length=6),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name="user",
            name="role",
            field=models.CharField(
                choices=[("advisor", "Advisor"), ("user", "User"), ("admin", "Admin")], default="user", max_length=10
            ),
        ),
    ]
