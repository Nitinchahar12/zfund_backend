from django.db import models
from django.contrib.auth.models import AbstractUser
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

class User(AbstractUser):
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    first_name = None  # type: ignore
    last_name = None  # type: ignore
    mobile = models.CharField(max_length=15, unique=True)
    role = models.CharField(max_length=10, choices=[('advisor', 'Advisor'), ('user', 'User'), ('admin', 'Admin')], default='user')
    otp = models.CharField(max_length=6)
    advisor = models.ForeignKey('self', on_delete=models.SET_NULL, null=True, related_name='clients')

    def get_absolute_url(self):
        return reverse("users:detail", kwargs={"username": self.username})
