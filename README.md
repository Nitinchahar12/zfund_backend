# zfund_backend

Backend Assignment for Zfund

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Black code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands

## To run the following software

## Instructions

- Create `virtualenv` for Python3 using `python3 -m venv env`.
- Activate your `virtualenv` using `source env/bin/activate`
- Once activated, install the requirements
- `pip install -r requirements/local.txt`
- Make sure you have `postgres` installed on your system
- If you have Postgres installed make sure you create a database with name `zfund_backend`
- Once all requirements are installed. Run Migrations
- `python manage.py migrate`
- When all migrations run successfully, please create a superuser.
- `python manage.py createsuperuser`
- Run `python mangage.py runserver`
- In the following application the login is via USERNAME and NOT EMAIL-ID
- Once done. Login into 127.0.0.1:8000/admin.


## Note :

API Docs - `http://127.0.0.1:8000/api/docs`
API Endpoint  & Explaination:
- 'http://127.0.0.1:8000/api/auth-token' - Gives token for authentication of the user.
- 'http://127.0.0.1:8000/api/signup/' - To add user as Advisor
- 'http://127.0.0.1:8000/api/add_client/' - To add client
- 'http://127.0.0.1:8000/api/view_clients/<int:advisor_id>/'- To view client by advisor users.
- 'http://127.0.0.1:8000/api/add_product/'- To add product.
- 'http://127.0.0.1:8000/api/purchase_product/' - To purchase product
